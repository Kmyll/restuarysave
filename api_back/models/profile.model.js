const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const profileSchema = new Schema({
  avatar: {type: Buffer},
  username: {type: String, required: true},
  bio: {type: String, required: true},
  visitedCountries: {type: String, required: true},
  generalConditions: {type: Boolean, required: true},

},{
  timestamps: true,
});

const Profile = mongoose.model('Profile', profileSchema);

module.exports = Profile;
