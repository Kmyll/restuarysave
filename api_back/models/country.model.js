const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const countrySchema = new Schema({
  country: {type: String, required: true},
  continent:  {type: String, required: true},
  currency: {type: String, required: true},
  gpi: {type: Number, required: true},
  capital: {type: String, required: true},
  latitude: {type: Number, required: true},
  longitude: {type: Number, required: true},
},{
  timestamps: true,
});

const Country = mongoose.model('Country', countrySchema);

module.exports = Country;
