const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const saltRounds = 10;
const secret = 'pro06';

const userSchema = new Schema({
	email: {type: String, unique: true, lowercase: true},
	password: String,
	firstName: String,
	lastName: String,
	bio: String,
	visitedCountries: String,
});

userSchema.pre("save", function(next){
    const user = this;
    console.log('user', user);

   	bcrypt.hash(user.password, saltRounds)
  		.then((hash, err)=>{
			console.log('hash  bcrypt ', hash);
	   		if (err) {

	            return next(err);
	        }
	        user.password = hash;
	        next();
	   	})
});


const UserModel = mongoose.model('users', userSchema);

module.exports = UserModel;