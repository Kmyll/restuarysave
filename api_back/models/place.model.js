const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const placeSchema = new Schema({
  placename: {type: String, required: true},
  country:  {type: String, required: true},
  continent:  {type: String, required: true},
  description: {type: String, required: true},
  activities: {type: Array, required: true},
  commutes: {type: Array, required: true},
  selectedOption: {type: String, required: true},
  selectedKidOption: {type: String, required: true},
  image: {type: Buffer, required: true},
},{
  timestamps: true,
});

const Place = mongoose.model('Place', placeSchema);

module.exports = Place;
