const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const secret = 'pro06';
const jwt = require('jsonwebtoken');

exports.signup = function(req, res, next) {
	console.log('user route ', req.body)


	User.findOne({email:req.body.email}, (err, existingUser)=> {
        if(err) {
			console.log('error bdd ', err)
			return next(err);
		}

		if (existingUser) {
			res.json({status: 422, error: "The email is already used"})
		} else {
		    const user = new User({
				email: req.body.email,
				password: req.body.password,
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				
			});

			user.save(function(err) {

				if(err) {
					console.log('error', err)
					return next(err);

				}

				res.json({status: 200, success: "Profile registered"});
			});
		}
    })

};

exports.signin = function(req, res, next) {
	console.log('user', req.body)
	//res.json({token: getTokenForUser(req.user)})
	User.findOne({email:req.body.email}, (err, existingUser)=> {
		if(err) {
			console.log('error bdd ', err)
			return next(err);
		}
		if(existingUser) {
			console.log('existingUser',existingUser)
			//bcrypt.compare(req.body.password, user[0].Password);
			bcrypt.compare(req.body.password, existingUser.password)
				.then((isMatch)=>{
					if(isMatch) {
						const payload = { email: req.body.email };
						const token = jwt.sign(payload, secret, {
				            expiresIn: '10d'
				         });
						res.json({status: 200, msg: 'good password', token: token});
					}
				})

		} else {

			res.json({status: 404, error: "No user font with this email"})

		}
	})
}

exports.checkToken = async function(req, res, next) {
	const token = req.headers['x-access-token']
	let decode = await jwt.verify(token, secret)
	let user = await User.findOne({email:decode.email})

	res.json({
	     		status: 200,
	            user: user,
	            msg: 'valid token'
	        })
}


exports.modify =  async (req, res, next) =>{
	var email = req.body.email;

	let update = await User.updateOne({email:email}, { 
		firstName: req.body.firstName, 
		lastName: req.body.lastName, 
		password: req.body.password, 
		bio: req.body.bio,
		visitedCountries: req.body.visitedCountries
	 })
	let user = await User.findOne({email:email});

	if(update.nModified === 0) {
		res.json({status: 404, error: 'No modification has been done'})
	}

	res.json({status: 200, msg: "Profile successfully updated!", user:user})
}
