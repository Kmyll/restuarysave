const router = require('express').Router();

let Profile = require('../models/profile.model');


router.route('/').get((req, res) => {
  Profile.find()
  .then(profiles => res.json(profiles))
  .then(profile => res.redirect('/profil'))
  .catch(err => res.status(400).json('Error: ' + err));

});

router.route('/add').post((req, res) => {
  const avatar = req.body.avatar;
  const username = req.body.username;
  const bio = req.body.bio;
  const visitedCountries = req.body.visitedCountries;
  const generalConditions = req.body.generalConditions;

  const newProfile = new Profile({
    avatar,
    username,
    bio,
    visitedCountries,
    generalConditions,
  });

newProfile.save()
    .then(() => res.json('Profile added !'))
    .catch(err => res.status(400)/json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Profile.findById(req.params.id)
  .then(profile => res.json(profile))
  .catch(err => res.status(400).json('Error:' + err));
});

router.route('/:id').delete((req, res) => {
  Profile.findByIdAndDelete(req.params.id)
    .then(() => res.json('Profile deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
  Profile.findById(req.params.id)
  .then(profile => {
    profile.avatar = req.body.avatar;
    profile.username = req.body.username;
    profile.bio = req.body.bio;
    profile.visitedCountries = req.body.visitedCountries;
    profile.generalConditions = req.body.generalConditions;

    profile.save()
    .then(() => res.json('Profile updated!'))
    .catch(err => res.status(400).json('Error:' + err));
  })
  .catch(err => res.status(400).json('Error:' + err));
});

module.exports = router;
