const router = require('express').Router();
let Country = require('../models/country.model');

router.route('/').get((req, res) => {
  Country.find()
  .then(countries => res.json(countries))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const country = req.body.country;
  const continent = req.body.continent;
  const currency = req.body.currency;
  const gpi = Number(req.body.gpi);
  const capital = req.body.gpi;
  const latitude = Number(req.body.latitude);
  const longitude = Number(req.body.longitude);


    const newCountry= new Country({
      country,
      continent,
      currency,
      gpi,
      capital,
      latitude,
      longitude,
    });

    newCountry.save()
      .then(() => res.json('Country added !'))
      .catch(err => res.status(400).json('Error: ' + err));
  });

  router.route('/:id').get((req, res) => {
    Country.findById(req.params.id)
    .then(country => res.json(country))
    .catch(err => res.status(400).json('Error:' + err));
  });

  router.route('/:id').delete((req, res) => {
    Country.findByIdAndDelete(req.params.id)
      .then(() => res.json('country deleted.'))
      .catch(err => res.status(400).json('Error: ' + err));
  });

  router.route('/update/:id').post((req, res) => {
    Country.findById(req.params.id)
    .then(country => {
      country.country = req.body.country;
      country.continent = req.body.continent;
      country.currency = req.body.currency;
      country.gpi = Number(req.body.gpi);
      country.capital = req.body.capital;
      country.latitude = Number(req.body.capital);
      country.longitude = Number(req.body.longitude);


      country.save()
      .then(() => res.json('Country updated!'))
      .catch(err => res.status(400).json('Error:' + err));
    })
    .catch(err => res.status(400).json('Error:' + err));
  });

  module.exports = router;
