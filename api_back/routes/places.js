const router = require('express').Router();

let Place = require('../models/place.model');


router.route('/').get((req, res) => {
  Place.find()
  .then(places => res.json(places))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const placename = req.body.placename;
  const country = req.body.country;
  const continent = req.body.continent;
  const description = req.body.description;
  const activities = req.body.activities;
  const commutes = req.body.commutes;
  const selectedOption = req.body.selectedOption;
  const selectedKidOption = req.body.selectedKidOption;
  const image = req.body.image;

  const newPlace = new Place({
    placename,
    country,
    continent,
    description,
    activities,
    commutes,
    selectedOption,
    selectedKidOption,
    image,

  });

newPlace.save()
    .then(() => res.json('Place added !'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
  Place.findById(req.params.id)
  .then(place => res.json(place))
  .catch(err => res.status(400).json('Error:' + err));
});

router.route('/:id').delete((req, res) => {
  Place.findByIdAndDelete(req.params.id)
    .then(() => res.json('Place deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/update/:id').post((req, res) => {
  Place.findById(req.params.id)
  .then(place => {
    place.placename = req.body.placename;
    place.country = req.body.country;
    place.continent = req.body.continent;
    place.description = req.body.description;
    place.activities = req.body.activities;
    place.commutes =  req.body.commutes;
    place.selectedOption =  req.body.selectedOption;
    place.selectedKidOption =  req.body.selectedKidOption;
    place.image = req.body.image;

    place.save()
    .then(() => res.json('Place updated!'))
    .catch(err => res.status(400).json('Error:' + err));
  })
  .catch(err => res.status(400).json('Error:' + err));
});

module.exports = router;
