const router = require('express').Router();
const multer = require('multer');

const multerConf = {
  storage : multer.diskStorage({
    destinatio : function(req, file, next){
      next(null, './public/images');
    },
    filename: function(req, file, next){
      console.log(file);
    }
  }),
};

/* get upload */

router.get('/upload', function(req, res){
  res.render('upload', {title:'upload file'})
  .then(upload => res.json(upload))
  .catch(err => res.status(400).json('Error:' + err));
});

/* post homepage */

router.post('/upload', multer(multerConf).single('file'), function(req, res){
  res.send('this is multer')
  .then(upload => res.json(upload))
  .catch(err => res.status(400).json('Error:' + err));
})


module.exports = router;
